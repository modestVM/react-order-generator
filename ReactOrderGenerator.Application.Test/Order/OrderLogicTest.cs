﻿using System.Collections.Generic;
using NUnit.Framework;
using ReactOrderGenerator.Application.Order.Logic;

namespace ReactOrderGenerator.Application.Test.Order
{
    [TestFixture]
    public class OrderLogicTest
    {
        [Test]
        [TestCase("WebTerm")]
        [TestCase("webterm")]
        [TestCase("WEBTERM")]
        public void Test_Get_Unique_OrderNumber_With_Valid_Names(string requestedName)
        {
            // Arrange
            var names = new List<string>()
            {
                "MODEST.WebTerm-001",
                "MODEST.WebTerm-25",
                "MODEST.WebTerm-29"
            };

            var logic = new OrderLogic();

            // Act
            var actual = logic.GetUniqueOrderNumber(names, requestedName);

            // Assert
            Assert.AreEqual(30, actual);
        }

        [Test]
        [TestCase("WebTerm")]
        [TestCase("webterm")]
        [TestCase("WEBTERM")]
        public void Test_Get_Unique_OrderNumber_With_InValid_Names(string requestedName)
        {
            // Arrange
            var names = new List<string>()
            {
                "MODEST.WebTerm-00s1",
                "MODEST.WebTerm--2z5-",
                "MODEST.WebTerm-Z29-"
            };

            var logic = new OrderLogic();

            // Act
            var actual = logic.GetUniqueOrderNumber(names, requestedName);

            // Assert
            Assert.AreEqual(1, actual);
        }

        [Test]
        [TestCase("WebTerm")]
        public void Test_Get_Unique_OrderNumber_When_New_OrderName(string requestedName)
        {
            // Arrange
            var names = new List<string>()
            {
                "MODEST.WebTerm-00s1",
                "MODEST.Sync--2z5-",
                "MODEST.Other-Z29-"
            };

            var logic = new OrderLogic();

            // Act
            var actual = logic.GetUniqueOrderNumber(names, requestedName);

            // Assert
            Assert.AreEqual(1, actual);
        }


        [Test]
        [TestCase("WebTerm")]
        public void Test_Get_Unique_OrderNumber_When_No_Names_Inside_Db(string requestedName)
        {
            // Arrange
            var names = new List<string>();

            var logic = new OrderLogic();

            // Act
            var actual = logic.GetUniqueOrderNumber(names, requestedName);

            // Assert
            Assert.AreEqual(1, actual);
        }

        [Test]
        [TestCase("")]
        public void Test_Get_Unique_OrderNumber_When_InValid_Requested_Name(string requestedName)
        {
            // Arrange
            var names = new List<string>()
            {
                "MODEST.WebTerm-230",
                "MODEST.Sync-25",
                "MODEST.Other-12"
            };

            var logic = new OrderLogic();

            // Act
            var actual = logic.GetUniqueOrderNumber(names, requestedName);

            // Assert
            Assert.AreEqual(1, actual);
        }
    }
}
