﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ReactOrderGenerator.Application.Common;
using ReactOrderGenerator.Application.Order;
using ReactOrderGenerator.Application.Order.Commands.InsertOrderCommand;
using ReactOrderGenerator.Application.Order.Commands.InsertOrderNameCommand;
using ReactOrderGenerator.Application.Order.Queries.GetOrderCountQuery;
using ReactOrderGenerator.Application.Order.Queries.GetOrderNamesQuery;
using ReactOrderGenerator.Application.Order.Queries.GetRecentOrdersQuery;

namespace ReactOrderGenerator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("names")]
        public async Task<List<string>> GetOrderNames()
        {
            var response = await _mediator.Send(new GetOrderNamesQuery());
            return response;
        }

        [HttpGet("recent-orders")]
        public async Task<RecentOrdersResponse> RecentOrders(int page)
        {
            var count = await _mediator.Send(new GetOrderCountQuery());
            var pagination = new Pagination(page, count);

            var recentOrders = await _mediator.Send(new GetRecentOrdersQuery(pagination));

            return recentOrders;
        }

        [HttpPost]
        public async Task<InsertResponse> InsertOrder([FromBody]InsertOrderRequest request)
        {
            if (request.IsCustomNameEnabled)
                await _mediator.Send(new InsertOrderNameCommand(request.Name));

            var response = await _mediator.Send(new InsertOrderCommand(request));
            return response;
        }
    }
}
