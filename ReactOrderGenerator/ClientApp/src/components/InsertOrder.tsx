import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as OrderStore from '../store/Order';
import { InsertOrderRequest } from '../models/requests/InsertOrderRequest';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { FormControl, Select, Typography, Slider, Input, Switch, FormControlLabel } from '@material-ui/core';
import { Label } from 'reactstrap';

type InsertOrderProps =
    OrderStore.InsertOrderState &
    typeof OrderStore.actionCreators &
    RouteComponentProps<{ startDateIndex: string }>;

class InsertOrder extends React.PureComponent<InsertOrderProps,
    {
        prefix: string,
        name: string,
        customName: string,
        isCustomNameEnabled: boolean,
        code: string,
        countToInsert: number
    }> {

    public componentDidMount() {
        this.ensureDataFetched();
    }
    private ensureDataFetched() {
        this.props.getOrderNames();
    }

    private inValidCustomName: boolean = true;
    private isCustomNameValidByRegex: boolean = true;

    private emptyString: string = '';
    private defaultName: string = 'None';

    private insertRequest?: InsertOrderRequest;

    constructor(props: any) {
        super(props);

        this.state = {
            prefix: this.emptyString,
            name: this.defaultName,
            customName: this.emptyString,
            code: this.emptyString,
            countToInsert: 10,
            isCustomNameEnabled: false
        }

        this.handlePrefixChange = this.handlePrefixChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleCustomNameChange = this.handleCustomNameChange.bind(this);
        this.handleIsCustomNameSwitchChange = this.handleIsCustomNameSwitchChange.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.handleCountToInsertChange = this.handleCountToInsertChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public handleIsCustomNameSwitchChange(event: any) {
        this.setState({ isCustomNameEnabled: event.target.checked });
    }

    public handleNameChange(event: any) {
        this.setState({ name: event.target.value });
    }
    public handleCustomNameChange(event: any) {
        this.setState({ customName: event.target.value });
        this.validateCustomName(event.target.value);

        if (!event.target.value) {
            this.inValidCustomName = true;
        }
        else {
            this.inValidCustomName = false;
        }
    }

    private validateCustomName(value: string) {
        var regExp = new RegExp("^[A-Za-z0-9]+$")
        var isValid = regExp.test(value);
        this.isCustomNameValidByRegex = isValid ? true : false;
    }
    public handlePrefixChange(event: any) {
        this.setState({ prefix: event.target.value });
    }
    public handleCodeChange(event: any) {
        this.setState({ code: event.target.value });
    }
    public handleCountToInsertChange(event: any) {
        this.setState({ countToInsert: event.target.value });
    }

    public handleSliderChange = (event: any, newValue: number | number[]) => {
        if (typeof newValue === 'number') {
            this.setState({ countToInsert: newValue });
        }
    };

    public handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        var value = Number(event.target.value);
        this.setState({ countToInsert: value });
    };

    public handleSubmit(event: any) {
        this.insertRequest = new InsertOrderRequest(
            this.state.prefix,
            this.state.name,
            this.state.isCustomNameEnabled,
            this.state.code,
            this.state.countToInsert);

        if (this.state.isCustomNameEnabled) {
            this.insertRequest.name = this.state.customName;
        }

        this.props.insertOrders(this.insertRequest);
        event.preventDefault();
    }

    public render() {
        const onePercentageMargin = {
            margin: '1%'
        }
        const countQniqueItemsDiv = {
            margin: '1%',
            width: '50%'
        }
        return (
            <React.Fragment>
                <h1>Insert order</h1>

                <h3>Example: Prefix.Name::Code </h3>

                <form onSubmit={this.handleSubmit}>
                    <div style={{ display: 'flex', flexDirection: 'column' }} >
                        <div style={onePercentageMargin}>
                            <TextField onChange={this.handlePrefixChange} id="outlined-basic" label="Prefix" />
                        </div>

                        <div style={onePercentageMargin}>
                            <div style={{ display: 'flex', flexDirection: 'row' }} >
                                {!this.state.isCustomNameEnabled && (
                                    <div>
                                        <FormControl required>
                                            <Select
                                                variant="outlined"
                                                native
                                                value={this.state.name}
                                                onChange={this.handleNameChange}
                                                name="name"
                                                inputProps={{ id: 'name-native-required' }} >

                                                <option value="None">None</option>
                                                {this.props.orderNames.map((name: string) => <option value={name}>{name}</option>)}
                                            </Select>
                                        </FormControl>
                                    </div>
                                )}

                                {this.state.isCustomNameEnabled && (
                                    <div>
                                        <FormControl >
                                            <TextField
                                                value={this.state.customName}
                                                variant="outlined"
                                                error={this.inValidCustomName}
                                                onChange={this.handleCustomNameChange}
                                                id="outlined-basic"
                                                label="Custom name *" />
                                        </FormControl>
                                    </div>
                                )}

                                <div style={{ marginLeft: '5%' }}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={this.state.isCustomNameEnabled}
                                                onChange={this.handleIsCustomNameSwitchChange}
                                                name="isCustomNameEnabled" />
                                        }
                                        label="Enable custom name"
                                    />
                                </div>
                            </div>
                        </div>

                        <div style={onePercentageMargin}>
                            <TextField onChange={this.handleCodeChange} id="outlined-basic" label="Code" />
                        </div>

                        <div style={countQniqueItemsDiv}>
                            <Typography id="input-slider" gutterBottom>
                                Count of unique items
                            </Typography>
                            <Grid container spacing={2} alignItems="center">
                                <Grid item xs>
                                    <Slider
                                        value={typeof this.state.countToInsert === 'number' ? this.state.countToInsert : 0}
                                        onChange={this.handleSliderChange}
                                        aria-labelledby="input-slider"
                                    />
                                </Grid>
                                <Grid item>
                                    <Input
                                        style={{ width: '42px' }}
                                        value={this.state.countToInsert}
                                        margin="dense"
                                        onChange={this.handleInputChange}
                                        inputProps={{
                                            min: 0,
                                            max: 100,
                                            type: 'number',
                                            'aria-labelledby': 'input-slider',
                                        }}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                        {!this.isCustomNameValidByRegex && this.state.isCustomNameEnabled && (
                            <div style={{ margin: '1%', color: 'red' }}>
                                <Label error="true"> - Custom name should contain laters or digits</Label>

                            </div>
                        )}
                        <div style={onePercentageMargin}>
                            <Button
                                disabled={this.state.isCustomNameEnabled && this.inValidCustomName}
                                type="submit"
                                variant="contained"
                                color="default">
                                Insert
                         </Button>
                        </div>
                    </div>
                </form>

            </React.Fragment>
        );
    }
};

export default connect(
    (state: ApplicationState) => state.order,
    OrderStore.actionCreators
)(InsertOrder as any);
