import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as RecentOrdersStore from '../store/RecentOrders';
import { OrderResponse } from '../models/responses/OrderResponse';

// At runtime, Redux will merge together...
type RecentOrdersProps =
  RecentOrdersStore.RecentOrdersState // ... state we've requested from the Redux store
  & typeof RecentOrdersStore.actionCreators // ... plus action creators we've requested
  & RouteComponentProps<{ startDateIndex: string }>; // ... plus incoming routing parameters


class RecentOrders extends React.PureComponent<RecentOrdersProps> {
  // This method is called when the component is first added to the document
  public componentDidMount() {
    this.ensureDataFetched();
  }

  // This method is called when the route parameters change
  public componentDidUpdate() {
    this.ensureDataFetched();
  }

  public render() {
    return (
      <React.Fragment>
        <h1 id="tabelLabel">Recent orders</h1>
        <p>This component demonstrates fetching data from the server and working with URL parameters.</p>
        {this.renderRecentOrdersTable()}
        {this.renderPagination()}
      </React.Fragment>
    );
  }

  private ensureDataFetched() {
    var index = '1';
    if(this.props.match){
      index = this.props.match.params.startDateIndex;
    }
    const startDateIndex = parseInt(index , 10) || 1;
    this.props.requestRecentOrders(startDateIndex);
  }

  private renderRecentOrdersTable() {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Name</th>
            <th>Code</th>
            <th>ExternalStatus</th>
            <th>BookStatus</th>
            <th>CreatedAt</th>
          </tr>
        </thead>
        <tbody>
          {this.props.recentOrders.map((order: OrderResponse) =>
            <tr key={order.name}>
              <td>{order.name}</td>
              <td>{order.code}</td>
              <td>{order.externalStatus}</td>
              <td>{order.bookStatus}</td>
              <td>{order.createdAt}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  private renderPagination() {
    const prevStartDateIndex = (this.props.startDateIndex || 1) - 1;
    const nextStartDateIndex = (this.props.startDateIndex || 1) + 1;

    return (
      <div className="d-flex justify-content-between">
        <Link className='btn btn-outline-secondary btn-sm' to={`/recent-orders/${prevStartDateIndex}`}>Previous</Link>
        {this.props.isLoading && <span>Loading...</span>}
        <Link className='btn btn-outline-secondary btn-sm' to={`/recent-orders/${nextStartDateIndex}`}>Next</Link>
      </div>
    );
  }
}

export default connect(
  (state: ApplicationState) => state.recentOrders, // Selects which state properties are merged into the component's props
  RecentOrdersStore.actionCreators // Selects which action creators are merged into the component's props
)(RecentOrders as any);
