export class InsertOrderRequest {
    constructor(
         prefix: string,
         name: string,
         isCustomNameEnabled: boolean,
         code: string, 
         countToInsert: number){
        this.prefix = prefix;
        this.name = name;
        this.isCustomNameEnabled = isCustomNameEnabled;
        this.code = code;
        this.countToInsert = countToInsert;
    }

    prefix: string;
    name: string;
    code: string;
    countToInsert: number;
    isCustomNameEnabled: boolean;
}
