export interface Pagination {
    skip: number;
    take: number;
    hasNextPage: boolean;
    hasPreviousPage: boolean;
}
