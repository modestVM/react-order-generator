export interface InsertResponse {
    errors: string[];
    hasErrors: boolean;
    countOfSuccessfullyInserted: number;
}
