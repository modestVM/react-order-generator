import { OrderResponse } from "./OrderResponse";
import { Pagination } from "../Pagination";

export interface RecentOrdersResponse {
    orders: OrderResponse[];
    pagination: Pagination;
}
