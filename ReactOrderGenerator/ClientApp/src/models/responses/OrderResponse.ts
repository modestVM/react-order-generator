export interface OrderResponse {
    name: string;
    code: string;
    externalStatus: string;
    bookStatus: string;
    createdAt: Date;
}
