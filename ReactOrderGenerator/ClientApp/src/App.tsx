import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import InsertOrder from './components/InsertOrder';
import RecentOrders from './components/RecentOrders';

import './custom.css'

export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/insert-order' component={InsertOrder} />
        <Route path='/recent-orders/:startDateIndex?' component={RecentOrders} />
    </Layout>
);
