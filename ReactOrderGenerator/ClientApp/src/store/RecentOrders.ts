import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { RecentOrdersResponse } from '../models/responses/RecentOrdersResponse';
import { OrderResponse } from '../models/responses/OrderResponse';
import { Pagination } from '../models/Pagination';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface RecentOrdersState {
    isLoading: boolean;
    startDateIndex?: number;
    recentOrders: OrderResponse[];
    pagination?: Pagination
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestRecentOrdersAction {
    type: 'REQUEST_RECENT_ORDERS';
    page: number;
}

interface ReceiveRecentOrdersAction {
    type: 'RECEIVE_RECENT_ORDERS';
    startDateIndex: number;
    recentOrders: OrderResponse[];
    pagination: Pagination
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestRecentOrdersAction | ReceiveRecentOrdersAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestRecentOrders: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.recentOrders && startDateIndex !== appState.recentOrders.startDateIndex) {
            fetch(`order/recent-orders` + '?page=' + startDateIndex)
                .then(response => response.json() as Promise<RecentOrdersResponse>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_RECENT_ORDERS', startDateIndex: startDateIndex, 
                    recentOrders: data.orders, pagination: data.pagination });
                });

            dispatch({ type: 'REQUEST_RECENT_ORDERS', page: startDateIndex });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: RecentOrdersState = { recentOrders: [], pagination: undefined, isLoading: false };

export const reducer: Reducer<RecentOrdersState> = (state: RecentOrdersState | undefined, incomingAction: Action)
: RecentOrdersState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_RECENT_ORDERS':
            return {
                startDateIndex: action.page,
                recentOrders: state.recentOrders,
                isLoading: true
            };
        case 'RECEIVE_RECENT_ORDERS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    recentOrders: action.recentOrders,
                    pagination: action.pagination,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};
