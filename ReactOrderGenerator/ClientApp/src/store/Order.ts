import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { InsertResponse } from '../models/responses/InsertResponse';
import { InsertOrderRequest } from '../models/requests/InsertOrderRequest';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface InsertOrderState {
    isLoading: boolean;
    insertedResponse?: InsertResponse;
    orderNames: string[];
}

export interface RequestInsertOrderAction {
    type: 'REQUEST_INSERT_ORDER',
    request: InsertOrderRequest
}
export interface ReceiveInsertedOrdersAction {
    type: 'RECEIVE_INSERTED_ORDERS',
    response: InsertResponse
}


interface RequestOrderNamessAction {
    type: 'REQUEST_ORDER_NAMES';
}

interface ReceiveOrderNamessAction {
    type: 'RECEIVE_ORDER_NAMES';
    names: string[]
}

export type KnownAction = RequestInsertOrderAction | ReceiveInsertedOrdersAction | ReceiveOrderNamessAction | RequestOrderNamessAction;

export const actionCreators = {
    insertOrders: (request: InsertOrderRequest): AppThunkAction<KnownAction> => (dispatch, getState) => {
        debugger;
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(request)
        };

        fetch(`order`, requestOptions)
            .then(response => response.json() as Promise<InsertResponse>)
            .then(data => {
                dispatch({ type: 'RECEIVE_INSERTED_ORDERS', response: data });
            });

        dispatch({ type: 'REQUEST_INSERT_ORDER', request: request });
    },
    getOrderNames: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        fetch(`order/names`)
            .then(response => response.json() as Promise<string[]>)
            .then(data => {
                dispatch({ type: 'RECEIVE_ORDER_NAMES', names: data });
            });

        dispatch({ type: 'REQUEST_ORDER_NAMES' });
    }
};

const unloadedState: InsertOrderState = { insertedResponse: undefined, isLoading: false, orderNames: [] };

export const reducer: Reducer<InsertOrderState> = (state: InsertOrderState | undefined, incomingAction: Action): InsertOrderState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INSERT_ORDER':
            return {
                isLoading: true,
                insertedResponse: state.insertedResponse,
                orderNames: state.orderNames
            };
        case 'RECEIVE_INSERTED_ORDERS':
            return {
                insertedResponse: action.response,
                isLoading: false,
                orderNames: state.orderNames
            };
        case 'RECEIVE_ORDER_NAMES':
            return {
                insertedResponse: state.insertedResponse,
                isLoading: false,
                orderNames: action.names
            };
        default:
            return state;
    }
};