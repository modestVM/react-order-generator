﻿using System;
using Dapper.Contrib.Extensions;

namespace ReactOrderGenerator.Application.Entities
{
    [Table("OrdKop")]
    public class Order : IEntity
    {
        [ExplicitKey]
        public string Nr { get; set; }
        public string Zkcode { get; set; }
        public string Referen { get; set; }
        public string ExtStatus { get; set; }
        public string Boekstatus { get; set; }
        public DateTime? SysModified { get; set; }
    }
}
