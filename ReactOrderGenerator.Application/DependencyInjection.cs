﻿using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReactOrderGenerator.Application.Order.Logic;
using ReactOrderGenerator.Application.Order.Repository;

namespace ReactOrderGenerator.Application
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IOrderLogic, OrderLogic>();
            services.AddTransient<IQueryOrderRepository, QueryOrderRepository>();
            services.AddTransient<ICommandOrderRepository, CommandOrderRepository>();

            services.AddAutoMapper(typeof(DependencyInjection));

            services.AddMediatR(Assembly.GetExecutingAssembly());

            var dbContext = new ConnectionStrings();
            configuration.Bind(dbContext.AppSettingConnectionKey, dbContext);

            services.Configure<ConnectionStrings>((s) =>
                {
                    s.SeniorConnectionString = dbContext.SeniorConnectionString;
                });
        }
    }
}
