﻿namespace ReactOrderGenerator.Application.Order
{
    public class InsertOrderRequest
    {
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public int CountToInsert { get; set; }

        public bool IsCustomNameEnabled { get; set; }
    }
}
