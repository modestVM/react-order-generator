﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReactOrderGenerator.Application.Order.Logic
{
    public class OrderLogic: IOrderLogic
    {
        private const string ModestPrefix = "MODEST";
        private const char SplitPredicate = '-';

        public int GetUniqueOrderNumber(List<string> orderNames, string requestedOrderName)
        {
            orderNames = orderNames.Where(x =>
                x.ToLowerInvariant()
                    .Contains(requestedOrderName.ToLowerInvariant())
            )
                .ToList();

            var valuesToTrim = $"{ModestPrefix}{requestedOrderName}{SplitPredicate}";

            var valuesToTrimAsArray = valuesToTrim.ToLowerInvariant().ToArray();

            var numbers = new List<int>();

            var trimmedNames = orderNames.Select(x => 
                x.ToLowerInvariant()
                    .Trim(valuesToTrimAsArray));

            foreach (var name in trimmedNames)
            {
                var isParsed = int.TryParse(name, out var result);
                if (isParsed)
                {
                    numbers.Add(result);
                }
            }

            var uniqueNumber = numbers.OrderByDescending(x => x).FirstOrDefault() + 1;

            return uniqueNumber;
        }

        public List<Entities.Order> GetOrdersToInsert(int uniqueOrderNumber, InsertOrderRequest request)
        {
            var orders = new List<Entities.Order>();

            var prefix = string.IsNullOrWhiteSpace(request.Prefix) ? ModestPrefix: request.Prefix;
            var code = string.IsNullOrWhiteSpace(request.Code) ? request.Name : request.Code;

            for (var i = default(int); i < request.CountToInsert; i++)
            {
                var order = new Entities.Order()
                {
                    Nr = $"{prefix}.{request.Name}-{uniqueOrderNumber}",
                    Zkcode = $"{prefix}.{code}-{uniqueOrderNumber}",
                    Referen = $"{prefix}.{code}-{uniqueOrderNumber}",
                    ExtStatus = "V",
                    Boekstatus = "W",
                    SysModified = DateTime.UtcNow
                };
                orders.Add(order);

                uniqueOrderNumber++;
            }

            return orders;
        }
    }
}
