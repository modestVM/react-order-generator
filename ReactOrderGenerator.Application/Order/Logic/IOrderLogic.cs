﻿using System.Collections.Generic;

namespace ReactOrderGenerator.Application.Order.Logic
{
    public interface IOrderLogic
    {
        int GetUniqueOrderNumber(List<string> orderNames, string requestedOrderName);
        List<Entities.Order> GetOrdersToInsert(int uniqueOrderNumber, InsertOrderRequest request);
    }
}
