﻿using ReactOrderGenerator.Application.Common;
using System.Collections.Generic;

namespace ReactOrderGenerator.Application.Order
{
    public class RecentOrdersResponse
    {
        public List<OrderDto> Orders { get; set; }
        public Pagination Pagination { get; set; }
    }
}
