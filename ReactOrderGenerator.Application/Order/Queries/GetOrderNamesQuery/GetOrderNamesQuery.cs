﻿using System.Collections.Generic;
using MediatR;

namespace ReactOrderGenerator.Application.Order.Queries.GetOrderNamesQuery
{
    public class GetOrderNamesQuery: IRequest<List<string>>
    {
    }
}
