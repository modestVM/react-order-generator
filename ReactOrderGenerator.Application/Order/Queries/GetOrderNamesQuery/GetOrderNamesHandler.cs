﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IniParser;
using MediatR;
using ReactOrderGenerator.Application.Ini;

namespace ReactOrderGenerator.Application.Order.Queries.GetOrderNamesQuery
{
    public class GetOrderNamesHandler: IRequestHandler<GetOrderNamesQuery, List<string>>
    {
        public Task<List<string>> Handle(GetOrderNamesQuery request, CancellationToken cancellationToken)
        {
            var parser = new FileIniDataParser();
            var data = parser.ReadFile(ConfigurationDefaults.ConfigName);

            var orders = data
                .Sections[ConfigurationDefaults.OrderNamesSection]
                .Select(x => x.Value)
                .ToList();

            return Task.FromResult(orders);
        }
    }
}
