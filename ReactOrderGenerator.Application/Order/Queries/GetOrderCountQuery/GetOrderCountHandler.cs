﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ReactOrderGenerator.Application.Order.Repository;

namespace ReactOrderGenerator.Application.Order.Queries.GetOrderCountQuery
{
    public class GetOrderCountHandler: IRequestHandler<GetOrderCountQuery, int>
    {
        private readonly IQueryOrderRepository _queryOrderRepository;

        public GetOrderCountHandler(
            IQueryOrderRepository queryOrderRepository)
        {
            _queryOrderRepository = queryOrderRepository;
        }
        public async Task<int> Handle(GetOrderCountQuery request, CancellationToken cancellationToken)
        {
            return await _queryOrderRepository.GetCountAsync();
        }
    }
}
