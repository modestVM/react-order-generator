﻿using MediatR;

namespace ReactOrderGenerator.Application.Order.Queries.GetOrderCountQuery
{
    public class GetOrderCountQuery : IRequest<int>
    {
    }
}
