﻿using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ReactOrderGenerator.Application.Order.Repository;

namespace ReactOrderGenerator.Application.Order.Queries.GetRecentOrdersQuery
{
    public class GetRecentOrdersHandler : IRequestHandler<GetRecentOrdersQuery, RecentOrdersResponse>
    {
        private readonly IMapper _mapper;
        private readonly IQueryOrderRepository _queryOrderRepository;

        public GetRecentOrdersHandler(
            IQueryOrderRepository queryOrderRepository,
            IMapper mapper)
        {
            _queryOrderRepository = queryOrderRepository;
            _mapper = mapper;
        }

        public async Task<RecentOrdersResponse> Handle(GetRecentOrdersQuery request, CancellationToken cancellationToken)
        {
            var recentOrders = await _queryOrderRepository.GetRecentOrdersAsync(request.Pagination.Skip, request.Pagination.Take);
            var mappedValues = _mapper.Map<List<OrderDto>>(recentOrders);

            var response = new RecentOrdersResponse
            {
                Orders = mappedValues, 
                Pagination = request.Pagination
            };

            return response;
        }
    }
}
