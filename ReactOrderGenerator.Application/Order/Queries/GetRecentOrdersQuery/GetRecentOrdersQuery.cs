﻿using MediatR;
using ReactOrderGenerator.Application.Common;

namespace ReactOrderGenerator.Application.Order.Queries.GetRecentOrdersQuery
{
    public class GetRecentOrdersQuery:IRequest<RecentOrdersResponse>
    {
        public GetRecentOrdersQuery(Pagination pagination)
        {
            Pagination = pagination;
        }
        public Pagination Pagination { get; private set; }
    }
}
