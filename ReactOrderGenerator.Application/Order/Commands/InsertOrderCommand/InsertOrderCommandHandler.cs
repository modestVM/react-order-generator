﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ReactOrderGenerator.Application.Common;
using ReactOrderGenerator.Application.Order.Logic;
using ReactOrderGenerator.Application.Order.Repository;

namespace ReactOrderGenerator.Application.Order.Commands.InsertOrderCommand
{
    public class InsertOrderCommandHandler : IRequestHandler<InsertOrderCommand, InsertResponse>
    {
        private readonly IOrderLogic _orderLogic;
        private readonly IQueryOrderRepository _queryOrderRepository;
        private readonly ICommandOrderRepository _commandOrderRepository;

        public InsertOrderCommandHandler(
            IOrderLogic orderLogic,
            IQueryOrderRepository queryOrderRepository,
            ICommandOrderRepository commandOrderRepository)
        {
            _orderLogic = orderLogic;
            _queryOrderRepository = queryOrderRepository;
            _commandOrderRepository = commandOrderRepository;
        }

        public async Task<InsertResponse> Handle(InsertOrderCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var response = new InsertResponse();
                Validate(request, response);

                if (response.HasErrors)
                    return response;

                var orderNames = await _queryOrderRepository.GetNamesByAsync(request.Model.Name);

                var uniqueOrderNumber = _orderLogic.GetUniqueOrderNumber(orderNames, request.Model.Name);

                var ordersToInsert = _orderLogic.GetOrdersToInsert(uniqueOrderNumber, request.Model);

                response.CountOfSuccessfullyInserted = await _commandOrderRepository.InsertRangeAsync(ordersToInsert);

                return response;

            }
            catch (Exception exception)
            {
                return new InsertResponse()
                {
                    Errors = new List<string>()
                    {
                        exception.Message
                    }
                };
            }
        }

        private void Validate(InsertOrderCommand request, InsertResponse response)
        {
            if (string.IsNullOrWhiteSpace(request.Model.Name))
            {
                response.Errors.Add("Name can not be null or empty");
            }
        }
    }
}
