﻿using MediatR;
using ReactOrderGenerator.Application.Common;

namespace ReactOrderGenerator.Application.Order.Commands.InsertOrderCommand
{
    public class InsertOrderCommand: IRequest<InsertResponse>
    {
        public InsertOrderCommand(InsertOrderRequest request)
        {
            Model = request;
        }
        public InsertOrderRequest Model { get; set; }
    }
}
