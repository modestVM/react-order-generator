﻿using MediatR;

namespace ReactOrderGenerator.Application.Order.Commands.InsertOrderNameCommand
{
    public class InsertOrderNameCommand: IRequest<bool>
    {
        public InsertOrderNameCommand(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
