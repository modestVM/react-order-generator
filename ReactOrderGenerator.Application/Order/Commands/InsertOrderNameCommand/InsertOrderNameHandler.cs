﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IniParser;
using MediatR;
using ReactOrderGenerator.Application.Ini;

namespace ReactOrderGenerator.Application.Order.Commands.InsertOrderNameCommand
{
    public class InsertOrderNameHandler: IRequestHandler<InsertOrderNameCommand, bool>
    {
        private const string DigitStringFormat = "N";

        public Task<bool> Handle(InsertOrderNameCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var parser = new FileIniDataParser();
                var data = parser.ReadFile(ConfigurationDefaults.ConfigName);

                var orderNames = data
                    .Sections[ConfigurationDefaults.OrderNamesSection]
                    .Select(x => x.Value)
                    .ToList();

                if (IsInsertAllowed(orderNames, request.Name))
                {
                    data.Sections[ConfigurationDefaults.OrderNamesSection]
                        .AddKey(
                            Guid.NewGuid().ToString(DigitStringFormat),
                            request.Name
                        );

                    parser.WriteFile(ConfigurationDefaults.ConfigName, data);
                }
            }
            catch
            {
                return Task.FromResult(false);
            }

            return Task.FromResult(true);
        }

        private bool IsInsertAllowed(IReadOnlyList<string> persistenceNames, string insertValue)
        {
            var isPresent = persistenceNames.Any(x =>
                x.ToLowerInvariant()
                    .Contains(insertValue.ToLowerInvariant()));

            return !isPresent;
        }
    }
}
