﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Options;

namespace ReactOrderGenerator.Application.Order.Repository
{
    public class CommandOrderRepository: ICommandOrderRepository
    {
        private readonly IOptions<ConnectionStrings> _connectionOptions;

        public CommandOrderRepository(IOptions<ConnectionStrings> connectionOptions)
        {
            _connectionOptions = connectionOptions;
        }

        public async Task<int> InsertRangeAsync(List<Entities.Order> orders)
        {
            await using var connection = new SqlConnection(_connectionOptions.Value.SeniorConnectionString);

            var countOfSuccessfullyInserted = connection.Insert(orders);

            return (int)countOfSuccessfullyInserted;
        }
    }
}
