﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactOrderGenerator.Application.Order.Repository
{
    public interface ICommandOrderRepository
    {
        Task<int> InsertRangeAsync(List<Entities.Order> orders);
    }
}
