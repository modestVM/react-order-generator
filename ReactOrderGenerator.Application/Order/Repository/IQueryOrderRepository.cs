﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactOrderGenerator.Application.Order.Repository
{
    public interface IQueryOrderRepository
    {
        Task<int> GetCountAsync();
        Task<List<string>> GetNamesByAsync(string orderName);
        Task<List<Entities.Order>> GetRecentOrdersAsync(int skip, int take);
    }
}
