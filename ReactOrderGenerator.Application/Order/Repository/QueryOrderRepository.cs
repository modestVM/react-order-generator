﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;

namespace ReactOrderGenerator.Application.Order.Repository
{
    public class QueryOrderRepository: IQueryOrderRepository
    {
        private readonly IOptions<ConnectionStrings> _connectionOptions;
        public QueryOrderRepository(IOptions<ConnectionStrings> connectionOptions)
        {
            _connectionOptions = connectionOptions;
        }

        public async Task<int> GetCountAsync()
        {
            await using var connection = new SqlConnection(_connectionOptions.Value.SeniorConnectionString);

            var count = await connection.ExecuteScalarAsync<int>("SELECT COUNT(*) FROM Ordkop");

            return count;
        }

        public async Task<List<string>> GetNamesByAsync(string orderName)
        {
            await using var connection = new SqlConnection(_connectionOptions.Value.SeniorConnectionString);

            const string getOrderNamesQuery = @"SELECT Nr FROM Ordkop 
                                               WHERE Nr LIKE '%' + @Name + '%'";

            var names = await connection.QueryAsync<string>(getOrderNamesQuery, new
            {
                Name = orderName
            });

            return names.ToList();
        }

        public async Task<List<Entities.Order>> GetRecentOrdersAsync(int skip, int take)
        {
            await using var connection = new SqlConnection(_connectionOptions.Value.SeniorConnectionString);

            const string recentOrdersQuery = @"SELECT
                                            Nr, 
                                            Zkcode,
                                            Referen,
                                            ExtStatus,
                                            Boekstatus,
                                            SysModified
                                            FROM Ordkop 
                                            ORDER BY SysModified DESC
                                            OFFSET (@Skip) ROWS FETCH NEXT (@Take) ROWS ONLY";

            var orders = await connection.QueryAsync<Entities.Order>(recentOrdersQuery, new
            {
                Skip = skip, 
                Take = take
            });

            return orders.ToList();
        }
    }
}
