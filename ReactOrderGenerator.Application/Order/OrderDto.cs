﻿using System;
using AutoMapper;

namespace ReactOrderGenerator.Application.Order
{
    public class OrderDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Referencing { get; set; }
        public string ExternalStatus { get; set; }
        public string BookStatus { get; set; }
        public DateTime? CreatedAt { get; set; }
    }

    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Entities.Order, OrderDto>()
                .ForMember(x=> x.Name, 
                    x=> x.MapFrom(s=>s.Nr))
                .ForMember(x => x.Code,
                    x => x.MapFrom(s => s.Zkcode))
                .ForMember(x => x.Referencing,
                    x => x.MapFrom(s => s.Referen))
                .ForMember(x => x.ExternalStatus,
                    x => x.MapFrom(s => s.ExtStatus))
                .ForMember(x => x.BookStatus,
                    x => x.MapFrom(s => s.Boekstatus))
                .ForMember(x => x.CreatedAt,
                    x => x.MapFrom(s => s.SysModified)); 
        }
    }
}
