﻿namespace ReactOrderGenerator.Application.Ini
{
    public static class ConfigurationDefaults
    {
        public static string ConfigName => "Configuration.ini";
        public static string OrderNamesSection => "OrderNames";
    }
}
