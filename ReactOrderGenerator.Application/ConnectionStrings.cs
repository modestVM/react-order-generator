﻿namespace ReactOrderGenerator.Application
{
    public sealed class ConnectionStrings
    {
        public string AppSettingConnectionKey => "ConnectionStrings";
        public string SeniorConnectionString { get; set; }
    }
}
