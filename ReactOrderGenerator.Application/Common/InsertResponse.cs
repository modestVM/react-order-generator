﻿using System.Collections.Generic;
using System.Linq;

namespace ReactOrderGenerator.Application.Common
{
    public class InsertResponse
    {
        public InsertResponse()
        {
            Errors = new List<string>();
        }

        public List<string> Errors { get; set; }
        public bool HasErrors => Errors.Any();

        public int CountOfSuccessfullyInserted { get; set; }
    }
}
