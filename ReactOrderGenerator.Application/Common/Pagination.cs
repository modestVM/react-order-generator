﻿namespace ReactOrderGenerator.Application.Common
{
    public class Pagination
    {
        private const int ItemsPerPage = 10;

        public Pagination(int page, long itemsCount) 
        {
            Skip = (page - 1) * ItemsPerPage;
            Take = ItemsPerPage;
            HasPreviousPage = page > 1;
            HasNextPage = itemsCount / ItemsPerPage > 1;
        }

        public int Skip { get; set; }
        public int Take { get; set; }
        public bool HasNextPage { get; private set; }
        public bool HasPreviousPage { get; private set; }
    }
}
